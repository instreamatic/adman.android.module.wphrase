package com.instreamatic.adman.module.wphrase;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.adman.statistic.LiveStatisticLoader;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.embedded.core.LanguageCode;
import com.instreamatic.embedded.core.PhraseSpot;
import com.instreamatic.player.IAudioPlayer;
import com.instreamatic.vast.model.VASTAd;
import com.sensory.speech.snsr.Snsr;
import com.sensory.speech.snsr.SnsrRC;
import com.sensory.speech.snsr.SnsrSession;

import java.lang.ref.WeakReference;

public class WPhraseModule extends BaseAdmanModule implements RequestEvent.Listener, PlayerEvent.Listener {
    final private static String TAG = "WPhraseModule";
    final public static String ID = WPhraseModule.class.getSimpleName();

    final private EventDispatcher dispatcher;
    protected WeakReference<Context> contextRef;

    //Phrase detect
    final private PhrasespotDetector phrasespotDetector;
    final private PhraseSpotEventListener phraseSpotEventListener = new PhraseSpotEventListener();
    final private ParametersDetectPhrase parametersDetectPhrase = new ParametersDetectPhrase();

    public WPhraseModule(Context context) {
        this.dispatcher = new EventDispatcher();
        this.contextRef = new WeakReference<>(context);

        phrasespotDetector = new PhrasespotDetector(phraseSpotEventListener);
        Snsr.init(context);  // Required for direct access to assets
    }

    @Override
    public String getId(){
        return ID;
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {RequestEvent.TYPE, PlayerEvent.TYPE};
    }

    //RequestEvent.Listener
    @Override
    public void onRequestEvent(RequestEvent event) {
        RequestEvent.Type type = event.getType();
        Log.d(TAG, "onRequestEvent: " + type);
        switch (type) {
            case SUCCESS:
                VASTAd ad =  (event.ads != null && (event.ads.size() > 0)) ? event.ads.get(0) : null;
                String typeVAST = ad != null ? ad.type : null;
                parametersDetectPhrase.used = (typeVAST != null && typeVAST.toLowerCase().equals("vor"));
                setAdmanParameters(parametersDetectPhrase.used, !parametersDetectPhrase.used);
                break;
        }
    }

    public void addListener(WphraseEvent.Listener listener) {
        dispatcher.addListener(WphraseEvent.TYPE, listener);
    }

    public void removeListener(WphraseEvent.Listener listener) {
        dispatcher.removeListener(WphraseEvent.TYPE, listener);
    }

    public void addModel(String model, LanguageCode language) {
        this.phrasespotDetector.addModel(model, language);
    }

    public LanguageCode getLanguage() {
        return this.phrasespotDetector.getLanguage();
    }

    public void setLanguage(LanguageCode value) {
        this.phrasespotDetector.setLanguage(value);
    }


    private void setAdmanParameters(boolean silence, boolean audio_focus) {
        IAdman adman = this.getAdman();
        if (adman == null) return;
        Bundle extra = new Bundle();
        extra.putBoolean("adman.need_silence_player", silence);
        adman.setParameters(extra);
        AdmanVoice admanVoice = adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
        if (admanVoice != null) {
            admanVoice.setAutoRecognition(!silence);
        }
    }

    @Override
    public void onPlayerEvent(PlayerEvent event) {
        PlayerEvent.Type type = event.getType();
        switch (type) {
            case PROGRESS:
                if (parametersDetectPhrase.used) {
                    IAudioPlayer player = getMediaPlayer();
                    int duration = player == null ? 0 : player.getDuration();
                    int position = player == null ? 0 : player.getPosition();
                    double factor = duration > 0 ? (double)position / duration : 0.;
                    if (factor > 0.35 && !parametersDetectPhrase.launched) {
                        Log.d(TAG, "PROGRESS: " + position + " - " + duration + " " + factor);
                        parametersDetectPhrase.launched = true;
                        parametersDetectPhrase.catched = false;
                        double timeout = duration - position;
                        boolean result = phrasespotDetector.start(timeout);
                        if (result) {
                            dispatcher.dispatch(new WphraseEvent(WphraseEvent.Type.START_DETECT, TAG));
                        }
                    }
                }
                break;
            case COMPLETE:
                if (parametersDetectPhrase.used) {
                    parametersDetectPhrase.launched = false;
                    phrasespotDetector.stop();
                    Log.d(TAG, "COMPLETE: useDetectPhrase " + parametersDetectPhrase.used + "; wasDetectPhrase " + parametersDetectPhrase.catched);
                    IAdman adman = this.getAdman();
                    if (adman != null) {
                        if (parametersDetectPhrase.catched) {
                            AdmanVoice admanVoice = adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
                            if (admanVoice != null) {
                                admanVoice.startRecognition();
                            }
                        }
                        else {
                            AdmanVoice admanVoice = adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
                            if (admanVoice != null) {
                                admanVoice.complete();
                            } else {
                                adman.skip();
                            }
                        }
                    }
                }
                break;
            case PAUSE:
            case FAILED:
                if (parametersDetectPhrase.used) {
                    parametersDetectPhrase.launched = false;
                    phrasespotDetector.stop();
                }
                break;
        }
    }

    class ParametersDetectPhrase {
        boolean used = false;
        boolean launched = false;
        boolean catched = false;
    }

    class PhraseSpotEventListener implements PhraseSpot.EventListener {
        public void onEvent(SnsrSession s, String key, SnsrRC res) {
            if (res == SnsrRC.STOP) {
                Log.d(TAG, "phrase: STOP or TIMED_OUT");
                dispatcher.dispatch(new WphraseEvent(WphraseEvent.Type.STOP_DETECT, TAG));
            }
        }

        public void onNLUEvent(SnsrSession s, String s_name, String s_value) {
        }

        public void onResult(SnsrSession s, String text) {
            Log.d(TAG, "phrase: text");
            parametersDetectPhrase.catched = true;
            dispatcher.dispatch(new WphraseEvent(WphraseEvent.Type.STOP_DETECT, TAG));
            sendAction("embedded_wake_word");
            playerComplete();
        }

        public void onError(SnsrSession s, String msg) {
        }

        public void onChangeProgress(SnsrSession s, double position, double duration) {
            //final double timeout = getDuration() - getPosition();
            //Log.d(TAG, "onChangeProgress, position: " + position + ", duration: " + duration + ", timeout: " + timeout );
        }
    }

    private void sendAction(final String action) {
        final WPhraseModule self = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                IAdman adman = self.getAdman();
                if (adman == null) return;
                new LiveStatisticLoader().send(adman, action);
            }
        }).start();
    }

    private IAudioPlayer getMediaPlayer() {
        IAdman adman = this.getAdman();
        return adman != null ? adman.getPlayer() : null;
    }

    private void playerComplete() {
        IAudioPlayer player = getMediaPlayer();
        if (player != null) {
            player.complete();
        }
    }
}
