package com.instreamatic.adman.module.wphrase;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.instreamatic.embedded.core.LanguageCode;
import com.instreamatic.embedded.core.PhraseSpot;
import com.instreamatic.embedded.core.PhraseSpotHub;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public final class PhrasespotDetector {
    final private static String TAG = PhrasespotDetector.class.getSimpleName();

    final private PhraseSpot.EventListener phraseSpotEventListener;
    Handler tHandler;

    private PhraseSpotHub mPhraseSpot;
    Map<LanguageCode, String> voiceModels = new HashMap<>();
    private LanguageCode languageCode;

    public PhrasespotDetector(PhraseSpot.EventListener eventListener) {
        this.phraseSpotEventListener = eventListener;
        if(tHandler == null) {
            tHandler = new Handler(Looper.getMainLooper());
        }
    }

    public void addModel(String model, LanguageCode language) {
        if (this.languageCode == null) {
            this.languageCode = language;
        }
        this.voiceModels.put(language, model);
    }

    public LanguageCode getLanguage() {
        return languageCode;
    }

    public void setLanguage(LanguageCode value) {
        this.languageCode = value;
    }

    public boolean start(final double timeout) {
        if (this.mPhraseSpot != null) {
            Log.d(TAG, "PhraseSpot is not null");
            return false;
        }
        tHandler.post(new Runnable() {
            @Override
            public void run()    {
                startDetectPhrase(timeout);
            }
        });
        return true;
    }

    public void stop() {
        tHandler.post(new Runnable() {
            @Override
            public void run()    {
                stopDetectPhrase();
            }
        });
    }

    private void startDetectPhrase(double timeout) {
        mPhraseSpot = new PhraseSpotHub(timeout);
        mPhraseSpot.addModels(this.voiceModels);
        if (this.languageCode != null) {
            mPhraseSpot.setLanguage(this.languageCode);
        }

        Bundle extra = new Bundle();
        extra.putBoolean(PhraseSpot.MODE_PHRASE_SPOT, true);
        mPhraseSpot.setParameters(extra);
        mPhraseSpot.setEventListener(this.phraseSpotEventListener);
        try {
            mPhraseSpot.start();
        } catch (Exception e) {
            Log.e(TAG, "startDetectPhrase ", e);
        }
    }

    private void stopDetectPhrase() {
        Log.d(TAG, "stop detect phrase");
        if (mPhraseSpot != null) {
            mPhraseSpot.setEventListener(null);
            mPhraseSpot.stop();
            mPhraseSpot = null;
        }
    }
}
