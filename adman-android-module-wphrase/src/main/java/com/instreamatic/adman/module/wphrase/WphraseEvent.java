package com.instreamatic.adman.module.wphrase;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public class WphraseEvent extends BaseEvent<WphraseEvent.Type, WphraseEvent.Listener> {

    final public static EventType<WphraseEvent.Type, WphraseEvent, Listener> TYPE = new EventType<WphraseEvent.Type, WphraseEvent, WphraseEvent.Listener>("ModuleWphrase") {
        @Override
        public void callListener(WphraseEvent event, WphraseEvent.Listener listener) {
            listener.onWphraseEvent(event);
        }
    };

    public enum Type {
        START_DETECT,
        STOP_DETECT,
    }
    final public String message;

    public WphraseEvent(WphraseEvent.Type type, String sender, String message) {
        super(type, sender);
        this.message = message;
    }

    public WphraseEvent(WphraseEvent.Type type, String sender) {
        this(type, sender, null);
    }

    @Override
    public EventType<WphraseEvent.Type, WphraseEvent, WphraseEvent.Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        void onWphraseEvent(WphraseEvent event);
    }
}
