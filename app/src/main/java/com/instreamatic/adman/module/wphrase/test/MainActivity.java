package com.instreamatic.adman.module.wphrase.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.wphrase.PhrasespotDetector;
import com.instreamatic.adman.module.wphrase.WPhraseModule;
import com.instreamatic.adman.module.wphrase.WphraseEvent;
import com.instreamatic.adman.view.core.BaseAdmanView;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.embedded.core.LanguageCode;
import com.instreamatic.embedded.core.PhraseSpot;
import com.instreamatic.embedded.recognition.VoiceRecognitionEmbedded;
import com.instreamatic.vast.model.VASTAd;
import com.sensory.speech.snsr.SnsrRC;
import com.sensory.speech.snsr.SnsrSession;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    final private static String TAG = "MainActivity";
    private IAdman adman;
    private AdmanEventListeners admanEventListeners;
    private DefaultAdmanView instreamaticAdsView;
    private View startView;
    private View loadingView;
    private View wakePhraseView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        admanEventListeners = new AdmanEventListeners();
        checkPermission();
        initAdman();
        wakePhraseView = findViewById(R.id.wakePhrase);
        loadingView = findViewById(R.id.loading);
        startView = findViewById(R.id.start);
        startView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adman.start();
                    }
                });
            }
        });
    }
    @Override
    protected void onPause() {
        clearAdsView();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initAdsView();
        if (adman!= null && adman.isPlaying()){
            showAdsView();
        }
    }

    private void initAdman() {
        AdmanRequest.Builder builder = getAdRequestAdman();
        adman = new Adman(this, builder.build());

        AdmanVoice admanVoice = new AdmanVoice(this);
        //for recognition on the device
        VoiceRecognitionEmbedded voiceRecognitionEmbedded = new VoiceRecognitionEmbedded(this);
        voiceRecognitionEmbedded.addModel("assets/models/large_instrmtc_ovr25_enUS.snsr", LanguageCode.ENGLISH_US);
        admanVoice.setVoiceRecognition(voiceRecognitionEmbedded);
        adman.bindModule(admanVoice);

        adman.getDispatcher().addListener(RequestEvent.TYPE, admanEventListeners);
        WPhraseModule wphraseModule = new WPhraseModule(this);
        wphraseModule.addListener(admanEventListeners);
        String pathModelPhraseSpot
                //= "spot-hbg-enUS-1.4.0-m.snsr";  // Hello Blue Genie
                = "spot_hey_ltalk_enUS.snsr";      // hey let's talk
                //= "spot_hey_listen_enUS.snsr";   // Hey, listen
        String triggerPath = new File("assets/models", pathModelPhraseSpot).toString();
        wphraseModule.addModel(triggerPath, LanguageCode.ENGLISH_US);
        adman.bindModule(wphraseModule);

    }

    private AdmanRequest.Builder getAdRequestAdman() {
        AdmanRequest.Builder request_builder = null;
        //vor
        request_builder = new AdmanRequest.Builder().setAdUrlAPI("https://x3.instreamatic.com/v5/vast/1976?microphone=1&type=vor");

        //voice
        /*
        request_builder = new AdmanRequest.Builder()
                .setSiteId(1249)
                .setRegion(Region.GLOBAL)
                .setType(Type.VOICE);
        /**/
        return  request_builder;
    }

    private void clearAdsView() {
        if (instreamaticAdsView != null) {
            instreamaticAdsView.close();
            adman.unbindModule(instreamaticAdsView);
            instreamaticAdsView = null;
        }
    }

    private void initAdsView() {
            instreamaticAdsView = new DefaultAdmanView(this);
            adman.bindModule(instreamaticAdsView);
    }

    private void showAdsView() {
        if (instreamaticAdsView != null) {
            instreamaticAdsView.show();
        }
    }


    /**
     * Events from Adman
     **/
    class AdmanEventListeners implements WphraseEvent.Listener, RequestEvent.Listener {
        @Override
        public void onWphraseEvent(WphraseEvent event) {
            WphraseEvent.Type type = event.getType();
            if (type == WphraseEvent.Type.START_DETECT || type == WphraseEvent.Type.STOP_DETECT) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        wakePhraseView.setVisibility(type == WphraseEvent.Type.START_DETECT ? View.VISIBLE : View.GONE);
                    }
                });
            }
        }
        //RequestEvent.Listener
        @Override
        public void onRequestEvent(RequestEvent event) {
            RequestEvent.Type type = event.getType();
            runOnUiThread(new Runnable() {
                public void run() {
                    startView.setVisibility(type == RequestEvent.Type.LOAD ? View.GONE: View.VISIBLE);
                    loadingView.setVisibility(type != RequestEvent.Type.LOAD ? View.GONE: View.VISIBLE);
                }
            });
        }
    }
    /**
     * Mic access
     **/
    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;
    private void checkPermission() {
        View mLayout = (View) findViewById(R.id.main_layout);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                try {
                    Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            new String[]{Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_REQUEST_RECORD_AUDIO);
                                }
                            })
                            .show();
                } catch (Exception ex) {

                }
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}